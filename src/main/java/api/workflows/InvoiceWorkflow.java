package api.workflows;

import api.InvoiceComparator;
import api.models.InvoiceResource;
import api.redis.RedisSetup;
import com.google.gson.Gson;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

public class InvoiceWorkflow {
    private final String SINGLE_INV_KEY = "invoiceId:";
    private Gson gson;
    private Jedis jedis;

    public InvoiceWorkflow() {
        gson = new Gson();
        jedis = RedisSetup.getConnection();
    }

    public ArrayList<InvoiceResource> getAllInvoices() {
        if(jedis == null){
            jedis = RedisSetup.getConnection();
        }
        Set<String> keys = jedis.keys(SINGLE_INV_KEY + "*");
        ArrayList<InvoiceResource> invoiceResources = new ArrayList<InvoiceResource>();
        for (String key : keys) {
            invoiceResources.add(gson.fromJson(jedis.get(key), InvoiceResource.class));
        }

        jedis.close();
        Collections.sort(invoiceResources, new InvoiceComparator());
        return invoiceResources;
    }

    public InvoiceResource getInvoice(String id) throws Exception {
        if(jedis == null){
            jedis = RedisSetup.getConnection();
        }
        String invoiceBlob = jedis.get(SINGLE_INV_KEY + id);
        if (invoiceBlob == null) {
            jedis.close();
            throw new Exception("Invoice does not exists: " + id);
        } else {
            jedis.close();
            return gson.fromJson(invoiceBlob, InvoiceResource.class);
        }
    }

    public InvoiceResource createInvoice(InvoiceResource invoice) throws Exception {
        if(jedis == null){
            jedis = RedisSetup.getConnection();
        }
        if(!validateInvoice(invoice)){
            throw new Exception("Invalid resource. All fields except for description must be filled. ");
        }
        if (jedis.get(SINGLE_INV_KEY + invoice.getInvoiceNo()) != null) {
            throw new Exception("Invoice number has already been created. ");
        }

        String blob = gson.toJson(invoice);
        String invoiceKey = SINGLE_INV_KEY + invoice.getInvoiceNo();
        jedis.set(invoiceKey, blob);
        InvoiceResource resource = gson.fromJson(jedis.get(invoiceKey), InvoiceResource.class);
        jedis.close();
        return resource;
    }

    public InvoiceResource updateInvoice(String id, InvoiceResource updatedInvoice) throws Exception {
        if(jedis == null){
            jedis = RedisSetup.getConnection();
        }
        if(!validateInvoice(updatedInvoice)){
            throw new Exception("Invalid resource. All fields except for description must be filled. ");
        }
        String invoiceKey = SINGLE_INV_KEY + id;
        String blob = jedis.get(invoiceKey);
        InvoiceResource invoiceResource = gson.fromJson(blob, InvoiceResource.class);
        if(!(invoiceResource.getInvoiceNo().equals(updatedInvoice.getInvoiceNo()))){
            throw new Exception("Invoice numbers cannot be changed");
        }

        blob = gson.toJson(updatedInvoice);
        jedis.set(invoiceKey, blob);

        InvoiceResource resource = gson.fromJson(jedis.get(invoiceKey), InvoiceResource.class);
        jedis.close();
        return resource;
    }

    public String deleteInvoice(String id) throws Exception {
        if(jedis == null){
            jedis = RedisSetup.getConnection();
        }
        jedis.del(SINGLE_INV_KEY + id);

        if (jedis.get(SINGLE_INV_KEY + id) != null) {
            throw new Exception("Delete failed.  Still found " + id);
        }
        String resource = jedis.get(SINGLE_INV_KEY + id);
        jedis.close();
        return resource;
    }

    private boolean validateInvoice(InvoiceResource invoice){
        if(invoice.getInvoiceNo() == null ||
                invoice.getCompanyName() == null ||
                invoice.getPrice() == null ||
                invoice.getStatus() == null ||
                invoice.getTypeOfWork() == null ||
                invoice.getDueDate() == null){
            return false;
        }else{
            return true;
        }
    }
}
